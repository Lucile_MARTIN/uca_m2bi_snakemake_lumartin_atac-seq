# uca_m2bi_snakemake_lumartin_atac-seq

## Description

Find here scripts and data set metadata from "STATegra, a comprehensive multi-omics dataset of B-cell differentiation in mouse"
David Gomez-Cabrero et al. 2019
https://doi.org/10.1038/s41597-019-0202-7. 

Objective is to use workflow language and adopt the FAIR principle for a reproducible science and finaly handling the ressources of the infrastructure to optimize the codes. The project will use the resources of the HPCformation cluster and snakemake tool on the ATAC-seq data.

Please, clone my git repository in your environment, and run the script Snakefile_script.sh :

```
git clone https://gitlab.com/Lucile_MARTIN/uca_m2bi_snakemake_lumartin_atac-seq.git
```

## Requirements

This script can run on a HPC infrastructure using Lmod to manage environment. 
Please, use the command line before script execution : 

```bash
[student03@ws2023-master ~]$ cd uca_m2bi_snakemake_lumartin_atac-seq
[student03@ws2023-master ~]$ mkdir log
[student03@ws2023-master ~]$ module purge
[student03@ws2023-master ~]$ module load conda/4.12.0
[student03@ws2023-master ~]$ conda activate
[student03@ws2023-master ~]$ module load gcc/8.1.0 python/3.7.1 snakemake/7.15.1
```

## Directory tree structure

```
.
|-- README.md
|-- config
|   `-- config.yaml
|-- log
|   `-- slurm-67989.out
|-- results
|   |-- fastqc_init
|   |   |-- ss50k_0h_R1_1_fastqc.html
|   |   |-- ss50k_0h_R1_1_fastqc.zip
|   |   ...
|   |-- fastqc_post
|   |   |-- ss50k_0h_R1_trimmed_1_P_fastqc.html
|   |   | ...
|   |   |-- ss50k_24h_R3_trimmed_1_P_fastqc.html
|   |   |-- ss50k_24h_R3_trimmed_1_P_fastqc.zip
|   |   |-- ss50k_24h_R3_trimmed_2_P_fastqc.html
|   |   `-- ss50k_24h_R3_trimmed_2_P_fastqc.zip
|   |-- mapping
|   |   |-- ss50k_0h_R1.bam
|   |   | ... 
|   |   `-- ss50k_24h_R3.bam
|   `-- trimmed
|       |-- ss50k_0h_R1_trimmed_1_P.fastq.gz
|       | ...    
|       |-- ss50k_24h_R3_trimmed_1_P.fastq.gz
|       |-- ss50k_24h_R3_trimmed_1_U.fastq.gz
|       |-- ss50k_24h_R3_trimmed_2_P.fastq.gz
|       `-- ss50k_24h_R3_trimmed_2_U.fastq.gz
`-- workflow
    |-- Snakefile.map.smk
    |-- Snakefile.qcInit.smk
    |-- Snakefile.qcPost.smk
    |-- Snakefile.trim.smk
    |-- Snakefile_script.sh
    |-- Snakefile_script.smk
    `-- envs
        |-- envConda1.yml
        `-- envConda2.yml
```

## Usages

For run all the rule, please execute the command:

```bash
[student03@ws2023-master ~]$ sbatch Snakefile_script.sh
```

or :

```bash
[student03@ws2023-master ~]$ snakemake --cores 8 --jobs 12 --use-conda --latency-wait 20 --snakefile workflow/Snakefile_script.smk --cluster "sbatch --mem {resources.mem_mb} --cpus-per-task {threads} --time={resources.time} --output=log/slurm-%j.out"
```

For launching a specific job, please, run the command (depend on --snakefile):

```bash
[student03@ws2023-master ~]$ snakemake --cores 8 --jobs 12 --use-conda --latency-wait 20 --snakefile workflow/Snakefile.qcInit.smk --cluster "sbatch --mem {resources.mem_mb} --cpus-per-task {threads} --time={resources.time} --output=log/slurm-%j.out"
```

## Authors and acknowledgment
Thank to my classmates and the professor, Mrs Nadia GOUE.

## License
Creative Commons Legal Code - CC0 1.0 Universal
