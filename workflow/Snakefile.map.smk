configfile: "config/config.yaml",

rule all:
    input:
        expand("results/mapping/{sample}.bam", sample=config["samples_trim"])


rule mapping:
    input:
        r1 = "results/trimmed/{sample}_trimmed_1_P.fastq.gz",
        r2 = "results/trimmed/{sample}_trimmed_2_P.fastq.gz",
    output:
        "results/mapping/{sample}.bam"
    params:
        index = "/home/users/shared/data/Mus_musculus/Mus_musculus_GRCm39/bowtie2/all"
    conda:
        "envs/envConda1.yml"
    threads:
        8
    resources:
        mem_mb=8000,
        time="01:00:00"
    shell:
        """
        bowtie2 -x {params.index} -1 {input.r1} -2 {input.r2} -X 2000 | samtools view -Sb > {output}
        """