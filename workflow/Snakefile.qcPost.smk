configfile: "config/config.yaml",

rule all:
    input:
        expand("results/fastqc_post/{sample}_trimmed_1_P.fastqc.zip", sample = config["samples_trim"]),
        expand("results/fastqc_post/{sample}_trimmed_2_P.fastqc.html", sample = config["samples_trim"]),
        expand("results/fastqc_post/{sample}_trimmed_1_P.fastqc.zip", sample = config["samples_trim"]),
        expand("results/fastqc_post/{sample}_trimmed_2_P.fastqc.html", sample = config["samples_trim"]),

rule fastqc_post_trim:
    input:
        expand("results/trimmed/{sample}_trimmed_1_P.fastq.gz", sample = config["samples_trim"]),
        expand("results/trimmed/{sample}_trimmed_2_P.fastq.gz", sample = config["samples_trim"]),
    output:
        expand("results/fastqc_post/{sample}_trimmed_1_P.fastqc.zip", sample = config["samples_trim"]), 
        expand("results/fastqc_post/{sample}_trimmed_2_P.fastqc.html", sample = config["samples_trim"]),
    conda:
        "envs/envConda1.yml"
    threads:
        8
    resources: 
        mem_mb=5000, 
        time="00:30:00"
    shell: "fastqc --outdir results/fastqc_post/ {input}"
