configfile: "config/config.yaml",

rule all:
    input:
        expand("results/fastqc_init/{sample}_fastqc.html", sample = config["samples"]),
        expand("results/trimmed/{sample}_trimmed_1_P.fastq.gz", sample=config["samples_trim"]),
        expand("results/trimmed/{sample}_trimmed_2_P.fastq.gz", sample=config["samples_trim"]),
        expand("results/fastqc_post/{sample}_trimmed_1_P.fastqc.zip", sample = config["samples_trim"]),
        expand("results/fastqc_post/{sample}_trimmed_2_P.fastqc.html", sample = config["samples_trim"]),
        expand("results/fastqc_post/{sample}_trimmed_1_P.fastqc.zip", sample = config["samples_trim"]),
        expand("results/fastqc_post/{sample}_trimmed_2_P.fastqc.html", sample = config["samples_trim"]),
        expand("results/mapping/{sample}.bam", sample=config["samples_trim"])

rule fastqc:
    input:
         expand("/home/users/shared/data/stategra/atac-seq/{sample}.fastq.gz", sample = config["samples"])
    output:
        expand("results/fastqc_init/{sample}_fastqc.zip", sample = config["samples"]), 
        expand("results/fastqc_init/{sample}_fastqc.html", sample = config["samples"])
    conda:
        "envs/envConda1.yml"
    resources: 
        mem_mb=5000, 
        time="00:30:00"
    shell: "fastqc --outdir results/fastqc_init/ {input}"

rule trimmomatic:
    input:
        fwd = "/home/users/shared/data/stategra/atac-seq/{sample}_1.fastq.gz",
        rev = "/home/users/shared/data/stategra/atac-seq/{sample}_2.fastq.gz",
    output:
        trimmed_fwd_paired = "results/trimmed/{sample}_trimmed_1_P.fastq.gz",
        trimmed_fwd_unpaired = "results/trimmed/{sample}_trimmed_1_U.fastq.gz",
        trimmed_rev_paired = "results/trimmed/{sample}_trimmed_2_P.fastq.gz",
        trimmed_rev_unpaired = "results/trimmed/{sample}_trimmed_2_U.fastq.gz",
    conda:
        "envs/envConda1.yml"
    threads:
        8
    resources: 
        mem_mb=5000, 
        time="00:30:00"
    shell:
        """
        trimmomatic PE -threads {threads} {input.fwd} {input.rev} \
        {output.trimmed_fwd_paired} {output.trimmed_fwd_unpaired} {output.trimmed_rev_paired} {output.trimmed_rev_unpaired} \
        ILLUMINACLIP:/home/users/shared/data/stategra/atac-seq/NexteraPE-PE.fa:2:30:10:2:keepBothReads \
        LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:33
        """

rule fastqc_post_trim:
    input:
        expand("results/trimmed/{sample}_trimmed_1_P.fastq.gz", sample = config["samples_trim"]),
        expand("results/trimmed/{sample}_trimmed_2_P.fastq.gz", sample = config["samples_trim"]),
    output:
        expand("results/fastqc_post/{sample}_trimmed_1_P.fastqc.zip", sample = config["samples_trim"]), 
        expand("results/fastqc_post/{sample}_trimmed_2_P.fastqc.html", sample = config["samples_trim"]),
    conda:
        "envs/envConda1.yml"
    threads:
        8
    resources: 
        mem_mb=5000, 
        time="00:30:00"
    shell: "fastqc --outdir results/fastqc_post/ {input}"

rule mapping:
    input:
        r1 = "results/trimmed/{sample}_trimmed_1_P.fastq.gz",
        r2 = "results/trimmed/{sample}_trimmed_2_P.fastq.gz",
    output:
        "results/mapping/{sample}.bam"
    params:
        index = "/home/users/shared/data/Mus_musculus/Mus_musculus_GRCm39/bowtie2/all"
    conda:
        "envs/envConda1.yml"
    threads:
        8
    resources:
        mem_mb=8000,
        time="01:00:00"
    shell:
        """
        bowtie2 -x {params.index} -1 {input.r1} -2 {input.r2} -X 2000 | samtools view -Sb > {output}
        """