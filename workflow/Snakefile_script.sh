#!/bin/bash

echo 'Date: 02/01/2024'
echo 'Object: ATAC-seq sample workflow for Stategra datasets showing job execution and dependency handling.'
echo 'Inputs: paths to scripts for quality control, triming, mapping and peak calling'
echo 'Outputs: trimmed fastq files, QC HTML files, mapping SAM/BAM files, output of peak calling'

echo -e "\n#Initial Quality Control --------------------------------------------------------------------------------"
snakemake --cores 8 --jobs 12 --use-conda --latency-wait 20 --snakefile workflow/Snakefile.qcInit.smk --cluster "sbatch --mem {resources.mem_mb} --cpus-per-task {threads} --time={resources.time} --output=log/slurm-%j.out"

echo -e "\n#Trimming --------------------------------------------------------------------------------"
snakemake --cores 8 --jobs 12 --use-conda --latency-wait 20 --snakefile workflow/Snakefile.trim.smk --cluster "sbatch --mem {resources.mem_mb} --cpus-per-task {threads} --time={resources.time} --output=log/slurm-%j.out"

echo -e "\n#Quality Control post-trimming --------------------------------------------------------------------------------"
snakemake --cores 8 --jobs 12 --use-conda --latency-wait 20 --snakefile workflow/Snakefile.qcPost.smk --cluster "sbatch --mem {resources.mem_mb} --cpus-per-task {threads} --time={resources.time} --output=log/slurm-%j.out"

echo -e "\n#Mapping --------------------------------------------------------------------------------"
snakemake --cores 8 --jobs 12 --use-conda --latency-wait 20 --snakefile workflow/Snakefile.qcInit.smk --cluster "sbatch --mem {resources.mem_mb} --cpus-per-task {threads} --time={resources.time} --output=log/slurm-%j.out"