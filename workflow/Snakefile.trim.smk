configfile: "config/config.yaml",

rule all:
    input:
        expand("results/trimmed/{sample}_trimmed_1_P.fastq.gz", sample=config["samples_trim"]),
        expand("results/trimmed/{sample}_trimmed_2_P.fastq.gz", sample=config["samples_trim"]),

rule trimmomatic:
    input:
        fwd = "/home/users/shared/data/stategra/atac-seq/{sample}_1.fastq.gz",
        rev = "/home/users/shared/data/stategra/atac-seq/{sample}_2.fastq.gz",
    output:
        trimmed_fwd_paired = "results/trimmed/{sample}_trimmed_1_P.fastq.gz",
        trimmed_fwd_unpaired = "results/trimmed/{sample}_trimmed_1_U.fastq.gz",
        trimmed_rev_paired = "results/trimmed/{sample}_trimmed_2_P.fastq.gz",
        trimmed_rev_unpaired = "results/trimmed/{sample}_trimmed_2_U.fastq.gz",
    conda:
        "envs/envConda1.yml"
    threads:
        8
    resources: 
        mem_mb=5000, 
        time="00:30:00"
    shell:
        """
        trimmomatic PE -threads {threads} {input.fwd} {input.rev} \
        {output.trimmed_fwd_paired} {output.trimmed_fwd_unpaired} {output.trimmed_rev_paired} {output.trimmed_rev_unpaired} \
        ILLUMINACLIP:/home/users/shared/data/stategra/atac-seq/NexteraPE-PE.fa:2:30:10:2:keepBothReads \
        LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:33
        """